
/**
 * The script is encapsulated in an self-executing anonymous function,
 * to avoid conflicts with other libraries
 */
 ;(function($) {


  /**
   * Declare 'use strict' to the more restrictive code and a bit safer,
   * sparing future problems
   */
  "use strict";

function enviarContato(caminho,dados,msgconfig){
      $.ajax({
        url:caminho,
        type:"POST",
        data:dados,
        dataType:'json',
        beforeSend: function(){
          msgconfig.status.addClass('hidden');
        },
        success: function(retorno){
          if(retorno.status){
            msgconfig.enviado.removeClass("hidden");
          }else{
            msgconfig.status.addClass('hidden');
            msgconfig.problema.removeClass('hidden');
          }
        },
        error: function(){
          msgconfig.problema.removeClass('hidden');
        }
      })
    }

    function validar_campo($campo){
      if($campo.val() == ""){
        $campo.addClass("form-error");
        $campo.parent().find(".help-block").addClass("valid-error");
        return false;
      }
      $campo.removeClass("form-error");
      $campo.parent().find(".help-block").removeClass("valid-error");
      return true;
    }

    /* form validation  formulario visita-banner*/
    $('#form-footer').submit(function(evento){
      evento.preventDefault();

      var situacao_do_form = true;

      var nome = $('#nome');
      var email = $('#email');
      

      situacao_do_form = validar_campo(nome);
      situacao_do_form = validar_campo(email);
      

      var msconfig = {
        status: $('.form-status'),
        enviado: $('.form-enviado'),
        problema: $('.form-problema')
      }


      if(situacao_do_form){
        enviarContato('mail.php',$(this).serialize(),msconfig);
      }

    })

    /* form validation  formulario visita-banner*/
    $('#formulario-principal').submit(function(evento){
      evento.preventDefault();

      var situacao_do_form = true;

      var nome = $('#nome-principal');
      var telefone = $('#telefone-principal');
      var email = $('#email-principal');
      var assunto = $('#assunto');
      

      situacao_do_form = validar_campo(nome);
      situacao_do_form = validar_campo(telefone);
      situacao_do_form = validar_campo(email);
      situacao_do_form = validar_campo(assunto);
      

      var msconfig = {
        status: $('.form-status2'),
        enviado: $('.form-enviado2'),
        problema: $('.form-problema2')
      }


      if(situacao_do_form){
        enviarContato('mail.php',$(this).serialize(),msconfig);
      }

    })

     $(".ancora").on('click', function(event) {

          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
              scrollTop:  ($(hash).offset().top-130)
            }, 800, function(){
         
              // Add hash (#) to URL when done scrolling (default click behavior)
              window.location.hash = hash;
            });
          } // End if
        });

     $('#media').carousel({
    pause: true,
    interval: false,
  });

})(jQuery);
